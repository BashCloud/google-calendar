import * as React from 'react';

import './TopBar.css';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import SearchIcon from '@mui/icons-material/Search';
import HelpIcon from '@mui/icons-material/HelpOutline';
import SettingsIcon from '@mui/icons-material/SettingsOutlined';
import AppsIcon from '@mui/icons-material/Apps';
import AccountCircle from '@mui/icons-material/AccountCircleOutlined';
import NavigateNext from '@mui/icons-material/NavigateNext';
import NavigateBefore from '@mui/icons-material/NavigateBefore';
import ArrowDropDown from '@mui/icons-material/ArrowDropDown';


import Divider from '@mui/material/Divider';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Check from '@mui/icons-material/Check';

import Popover from '@mui/material/Popover';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import StaticDatePicker from '@mui/lab/StaticDatePicker';


import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme({
    palette: {
      neutral: {
        main: '#3c4043',
      },
    },
});

declare module '@mui/material/styles' {
    interface Palette {
        neutral: Palette['primary'];
    }

    // allow configuration using `createTheme`
    interface PaletteOptions {
        neutral?: PaletteOptions['primary'];
    }
}

// Update the Button's color prop options
declare module '@mui/material/Button' {
    interface ButtonPropsColorOverrides {
        neutral: true;
    }
}





interface TopBarProps {
    onNext: Function,
    onPrev: Function,
    setWeekStartDate: Function,
    weekStartDate: Date
}

const DayCell: React.FC<TopBarProps> = ({
    onNext,
    onPrev,
    setWeekStartDate,
    weekStartDate
}) => {
    const [value, setValue] = React.useState<Date | null>(new Date());
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [anchorCalendarEl, setAnchorCalendarEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };

    const openCalendar = Boolean(anchorCalendarEl);
    const handleCalendarClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorCalendarEl(event.currentTarget);
    };
    const handleCalendarClose = () => {
        setAnchorCalendarEl(null);
    };
    const monthNames = ["January", "Febraury", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const getActiveMonthString = (weekStartDate: Date) => {
        const weekEndDate = new Date((weekStartDate.getTime() + 86400000 * 6));
        const weekStartMonth = monthNames[ weekStartDate.getMonth()] 
        const weekEndMonth = monthNames[ weekEndDate.getMonth()] 
        const weekStartYear = weekStartDate.getFullYear()
        const weekEndYear = weekEndDate.getFullYear()
        return weekStartMonth === weekEndMonth ? 
            `${weekStartMonth} ${weekStartYear}` : 
            (weekStartYear === weekEndYear ?
                `${weekStartMonth.substring(0,3)} - ${weekEndMonth.substring(0,3)} ${weekStartYear}` :
                `${weekStartMonth.substring(0,3)} ${weekStartYear.toString().substring(2,4)} - ${weekEndMonth.substring(0,3)} ${weekEndYear.toString().substring(2,4)}`)
    }
    return (

        <div className="top-bar">
            <div className="top-bar__left">
                <IconButton size="large" style={{margin: '0px 4px'}}>
                    <MenuIcon />
                </IconButton>
                <img height="40px" width="40px" src="https://ssl.gstatic.com/calendar/images/dynamiclogo_2020q4/calendar_10_2x.png" alt="Google Calendar" />
                <div className="app-name">Calendar</div>
                <ThemeProvider theme={theme}>
                    <Button  color="neutral" variant="outlined" onClick={() => setWeekStartDate(new Date())} style={{marginLeft: '48px', marginRight: '12px', borderColor: '#cccccc', textTransform: 'capitalize'}}>
                        Today
                    </Button>
                </ThemeProvider>
                <IconButton onClick={() => onPrev()}>
                    <NavigateBefore />
                </IconButton>
                <IconButton onClick={() => onNext()}>
                    <NavigateNext />
                </IconButton>

                <ThemeProvider theme={theme}>
                    <Button color="neutral" endIcon={<ArrowDropDown />} onClick={handleCalendarClick} style={{marginRight: '12px', marginLeft: '12px', borderColor: '#cccccc', textTransform: 'capitalize', fontSize: '18px', fontWeight: 300}}>
                        {getActiveMonthString(weekStartDate)}
                    </Button>
                </ThemeProvider>
                <Popover 
                    anchorEl={anchorCalendarEl}
                    onClose={handleCalendarClose}
                    open={openCalendar}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    >
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <StaticDatePicker
                            displayStaticWrapperAs="desktop"
                            value={value}
                            onChange={(newValue) => {
                                setValue(newValue);
                                setWeekStartDate(newValue);
                                handleCalendarClose();

                            }}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Popover>


            </div>
            <div className="top-bar__right">
                <IconButton>
                    <SearchIcon />
                </IconButton>
                <IconButton>
                    <HelpIcon />
                </IconButton>
                <IconButton>
                    <SettingsIcon />
                </IconButton>
                <ThemeProvider theme={theme}>
                    <Button color="neutral" variant="outlined" endIcon={<ArrowDropDown />} onClick={handleClick} style={{marginRight: '32px', marginLeft: '12px', borderColor: '#cccccc', textTransform: 'capitalize'}}>
                        Week
                    </Button>
                </ThemeProvider>
                <Menu
                    anchorEl={anchorEl}
                    open={open}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleClose}>
                        <ListItemText>Day</ListItemText>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <ListItemText>Week</ListItemText>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <ListItemText>Month</ListItemText>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <ListItemText>Schedule</ListItemText>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <ListItemText>4 days</ListItemText>
                    </MenuItem>

                    <Divider />
                    <MenuItem onClick={handleClose}>
                        <ListItemIcon>
                            <Check />
                        </ListItemIcon>
                        Show Weekends
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                        <ListItemIcon>
                            <Check />
                        </ListItemIcon>
                        Show declined events
                    </MenuItem>
                </Menu>
                <IconButton size="large" >
                    <AppsIcon />
                </IconButton>
                <IconButton >
                    <AccountCircle fontSize="large"/>
                </IconButton>
            </div>


        </div>
    )
}

export default DayCell;