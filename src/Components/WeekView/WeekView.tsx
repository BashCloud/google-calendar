import { useEffect, useState } from "react";
import useLocalDB, {IEvent} from "../../Hooks/useLocalDB";
import { DayColumnHeader } from "../DayColumnHeader";
import { DayColumn } from "../DayColumn";
import { TimeColumn } from "../TimeColumn";
import { Loader } from "../Loader";
import './WeekView.css';

interface IDayCell {
    events: IEvent[];
    allDayEvents: IEvent[];
    date: Date;
}

interface IWeekViewProps {
    weekStartDate: Date;
}


const WeekView: React.FC<IWeekViewProps> = ({ weekStartDate }) => {
    const [weekCells, setWeekCells] = useState<IDayCell[]>([]);
    const { calendarEvents } = useLocalDB();

    const checkDateEquality = (d1: Date, d2: Date): boolean => {
        return d1.getFullYear() === d2.getFullYear() && d1.getDate() === d2.getDate() && d1.getMonth() === d2.getMonth();
    }
    const getEventsByDate = (date: Date) => {
        const events: IEvent[] = [];
        const allDayEvents: IEvent[] = [];
        calendarEvents?.forEach((ev) => {
            if (
                checkDateEquality(ev.start_date, date) 
                // || checkDateEquality(ev.end_date, date)
            ) {
                if(ev.start.dateTime) {
                    events.push(ev);

                } else {
                    allDayEvents.push(ev);
                }
            }
        })
        return {
            events,
            allDayEvents
        }
    }


    useEffect(() => {
        let currDate = new Date();
        let cells: IDayCell[] = []
        if (calendarEvents) {
            for (let day = 0; day < 7; day++) {
                currDate = new Date((weekStartDate.getTime() + 86400000 * day));
                cells.push({ ...getEventsByDate(currDate), 'date': new Date(currDate.getTime()) })
            }
            setWeekCells(cells);
            console.log(cells);
        }
    }, [calendarEvents, weekStartDate])
    return (
        <div className="week__wrap">
            <div className="calendar-header">
                <div className="add-event__cell">
                    <div className="add-event__btn ">
                        <svg width="36" height="36" viewBox="0 0 36 36"><path fill="#34A853" d="M16 16v14h4V20z"></path><path fill="#4285F4" d="M30 16H20l-4 4h14z"></path><path fill="#FBBC05" d="M6 16v4h10l4-4z"></path><path fill="#EA4335" d="M20 16V6h-4v14z"></path><path fill="none" d="M0 0h36v36H0z"></path></svg>
                    </div>
                </div>
                <div className="days-row">
                    {
                        weekCells.map(({ allDayEvents, date }) =>
                            <DayColumnHeader key={date.getDate()} date={date} allDayEvents={allDayEvents} />
                        )
                    }
                </div>
            </div>
            <div className="calendar-body">
                <TimeColumn />
                <div className="day-columns">
                    {
                        weekCells.map(({ events, date }) =>
                            <DayColumn key={date.getDate()} events={events} date={date}  />
                        )
                    }
                </div>
            </div>
        </div>
    )
}

export default WeekView;