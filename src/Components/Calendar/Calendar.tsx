import { useEffect, useState } from 'react';
import { TopBar } from "../TopBar";
import { WeekView } from '../WeekView';

import './Calendar.css';

const weekDays = [0, 1, 2, 3, 4, 5, 6];

const Calendar = () => {
    const today = new Date();
    const [weekStartDate, setWeekStartDate] = useState<Date>(today);
    useEffect(() => {
        let startDate = new Date();
        startDate.setDate(today.getDate() - (weekDays.indexOf(today.getDay())));
        setWeekStartDate(startDate);
    }, []);

    const handleNextClick = () => {
        let startDate = new Date((weekStartDate.getTime() + 86400000 * 7));
        setWeekStartDate(startDate);
    }
    const handlePrevClick = () => {
        let startDate = new Date((weekStartDate.getTime() - 86400000 * 7));
        setWeekStartDate(startDate);
    }
    const updateWeekStartDate = (newDate: Date) => {
        let startDate = new Date();
        startDate.setDate(newDate.getDate() - (weekDays.indexOf(newDate.getDay())))
        startDate.setFullYear(newDate.getFullYear());
        setWeekStartDate(startDate);
    }
    return (
        <div className="page-wrap">
            <TopBar onNext={handleNextClick} onPrev={handlePrevClick} setWeekStartDate={updateWeekStartDate} weekStartDate={weekStartDate}/>
            <WeekView weekStartDate={weekStartDate} />
        </div>
    );
};

export default Calendar;