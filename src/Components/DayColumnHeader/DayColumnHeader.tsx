import './DayColumnHeader.css';
import { IEvent } from "../../Hooks/useLocalDB";

interface DayColumnHeaderProps {
    date: Date;
    allDayEvents: IEvent[];
}

const DayColumnHeader: React.FC<DayColumnHeaderProps> = ({ date, allDayEvents }) => {
    const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    const today = new Date();
    let isToday = (today.getFullYear() === date.getFullYear() && today.getDate() === date.getDate() && today.getMonth() === date.getMonth())? true : false;
    return (

        <div className={`day-cell ${isToday? '--today':''}`}>
            <div className="day-name">{dayNames[date.getDay()]}</div>
            <div className="day-date">{date.getDate()}</div>
            {   
                allDayEvents.map( ev => {
                    return (
                        <div className="event-card --all-day" key={ev.id} >
                            <b>{ev.summary}</b>
                        </div>
                    )
                })
            }
            <div className="day__seperator"></div>
        </div>
    )
}

export default DayColumnHeader;