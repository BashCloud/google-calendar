import './TimeColumn.css';

const TimeColumn: React.FC = () => {
    const dayTime = [
        "", "1 AM", "2AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM",
        "12 PM", "1 PM", "2PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM",
    ];
    return (
        <div className="time-column">
            <div className="time-column__value --timezone">
                GMT+05:30
            </div>
            {dayTime.map( (time => {
                return (
                    <div className="time-column__cell" key={time}>
                        <div className="time-column__value">
                            {time}
                        </div>
                    </div>
                )
            }))}

        </div>
    )
}

export default TimeColumn;