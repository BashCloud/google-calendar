import './DayColumn.css';
import { IEvent } from "../../Hooks/useLocalDB";
// import { EventCard } from "../EventCard";

interface DayCellProps {
    events: IEvent[];
    date: Date;
}

function twoDigitFormat(num: Number, len = 2) {
    return `${num}`.padStart(len, '0');
}

const DayColumn: React.FC<DayCellProps> = ({ events, date }) => {
    const dayTime = [
        "12 AM", "1 AM", "2AM", "3 AM", "4 AM", "5 AM", "6 AM", "7 AM", "8 AM", "9 AM", "10 AM", "11 AM",
        "12 PM", "1 PM", "2PM", "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM", "11 PM",
    ];
    // console.log(events);
    const today = new Date();
    let isToday = (today.getFullYear() === date.getFullYear() && today.getDate() === date.getDate() && today.getMonth() === date.getMonth())? true : false;
    return (
        <div className="day-column">
            {dayTime.map( (time => {
                return (
                    <div className="day-time-cell" key={time}>
                        {/* {time} */}
                    </div>
                )
            }))}
            {   
                events.map( ev => {
                    const startHours = ev.start_date.getHours();
                    const startMinutes = ev.start_date.getMinutes();
                    const topOffset = (startHours * 50) + (startMinutes * 50/60);

                    const endHours = ev.end_date.getHours();
                    const endMinutes = ev.end_date.getMinutes();
                    const endOffset = (endHours * 50) + (endMinutes * 50/60);

                    const height = endOffset - topOffset;
                    return (
                        <div className="event-card" key={ev.id} style={{top: `${topOffset}px`, height: `${height}px` }} >
                            <b>{ev.summary}</b>
                            <br/>
                            {ev.start.dateTime && <span>
                                {twoDigitFormat(startHours)}:{twoDigitFormat(startMinutes)} - {twoDigitFormat(endHours)}:{twoDigitFormat(endMinutes)}
                            </span>}
                        </div>
                    )
                })
            }
            {isToday && <div className="current-time__line" style={{top: `${today.getHours() * 50 + (today.getMinutes() * 50/60) }px`}}></div>}
            <div className="day-column__seperator"></div>
        </div>
    )
}

export default DayColumn;