import Localbase from 'localbase';
import { useEffect, useState } from 'react';
import sampleData from "../stub_v2.json";

interface IUser {
    email: string;
    self: boolean;
}

export interface IStartOrEndDate {
    date?: string;
    dateTime?: string;
    timezone?: string;
}

export interface IReminders {
    useDefault: boolean;
}
export interface IEvent {
    kind: string;
    etag: string;
    id: string;
    status: string;
    htmlLink: string;
    created: string;
    updated: string;
    summary: string;
    creator: IUser;
    organizer: IUser;
    start_date: Date;
    end_date: Date;
    start: IStartOrEndDate;
    end: IStartOrEndDate;
    transparency: string;
    iCalUID: string;
    sequence: number;
    reminders: IReminders;
    eventType: string;
}

const useLocalDB = () => {
    const calendarDB = new Localbase('calendarDB');
    const [calendarEvents, setCalendarEvents] = useState<IEvent[]>();

    const getCalendarEvents = async () => {
        let events: IEvent[] = await calendarDB.collection('calendarEvents').get();
        return events;
    }

    const initializeCalendarDB = async () => {
        let events = await getCalendarEvents();

        if (events.length === 0) {
            let data = null;
            try {
                data = await (await (fetch('http://localhost:4000/getCalendarEvents'))).json();
                console.log(data);
            } catch (err) {
                console.log(err);
                data = sampleData; // API bypass for Static Depoyment.
            }


            data['items'].forEach(async (event: any) => {
                await calendarDB.collection('calendarEvents').add(event);
            })
            data.items.forEach((ev: any) => {
                ev['start_date'] = new Date(ev.start.dateTime || ev.start.date);
                ev['end_date'] = new Date(ev.end.dateTime || ev.end.date);
            });
            setCalendarEvents(data.items);
            return;
        }
        // events = await getCalendarEvents();
        setCalendarEvents(events);
    }

    useEffect(() => {
        initializeCalendarDB();
    }, [])

    return {
        calendarEvents
    }
}

export default useLocalDB;